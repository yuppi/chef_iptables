#
# Cookbook Name:: iptables
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#

#---------------------
# iptablesを無効にする
#---------------------
service 'iptables' do
    action [:disable, :stop]
end